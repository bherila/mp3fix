﻿using Delimon.Win32.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mp3fix
{

	public abstract class EmbedExtractAction
	{
		protected ListViewItem li;
		public abstract void ExecuteAction();

		public ListViewItem GetListItem()
		{
			var li = new ListViewItem(DisplayAction);
			li.SubItems.Add(DisplayDirectory);
			li.SubItems.Add(DisplaySource);
			li.SubItems.Add(DisplayTarget);
			li.SubItems.Add(DisplayType);
			if (!Enabled)
				li.ForeColor = System.Drawing.SystemColors.GrayText;
			return li;
		}

		[Browsable(false)] public abstract string DisplayAction { get; }
		[Browsable(false)] public abstract string DisplayDirectory { get; }
		[Browsable(false)] public abstract string DisplaySource { get; }
		[Browsable(false)] public abstract string DisplayTarget { get; }
		[Browsable(false)] public abstract string DisplayType { get; }

		public abstract byte[] GetImagePreview();

		public bool Enabled { get; set; }
		public int ExecuteOrder;
		public int CreationOrder;
		public Song AssociatedSong;
	}

	public class EmbedAction : EmbedExtractAction
	{
		public FileInfo file;

		[DefaultValue(TagLib.PictureType.FrontCover)]
		public TagLib.PictureType PhotoType { get; set; }

		[DefaultValue("")]
		public String Description { get; set; }

		public EmbedAction(FileInfo file, Song target)
		{
			this.file = file;
			this.AssociatedSong = target;
			this.PhotoType = TagLib.PictureType.FrontCover;
			this.Description = string.Empty;
			ExecuteOrder = 1;
		}

		public override void ExecuteAction()
		{
			if (Enabled)
			{
				List<TagLib.IPicture> pics = new List<TagLib.IPicture>(AssociatedSong.mp3.Tag.Pictures);
				
				var pp = new TagLib.Picture(new ZetaFileAbstraction(file));
				pp.Type = PhotoType;
				pp.Description = Description;
				var fl = file.Name.ToLower();
				if (fl.EndsWith(".jpg") || fl.EndsWith(".jpeg"))
					pp.MimeType = "image/jpeg";
				else if (fl.EndsWith(".png"))
					pp.MimeType = "image/png";
				else if (fl.EndsWith(".gif"))
					pp.MimeType = "image/gif";
				else if (fl.EndsWith(".bmp"))
					pp.MimeType = "image/bmp";
				pics.Add(pp);

				AssociatedSong.mp3.Tag.Pictures = pics.ToArray();
				AssociatedSong.Dirty = true;
			}
		}


		public override string DisplayAction
		{
			get { return "Embed"; }
		}

		public override string DisplayDirectory
		{
			get { return AssociatedSong.File.Directory.FullName; }
		}

		public override string DisplaySource
		{
			get { return file.Name; }
		}

		public override string DisplayTarget
		{
			get { return String.Format("+<tag:{0}> ", AssociatedSong.mp3.Tag.Pictures.Length) + AssociatedSong.File.Name; }
		}

		public override string DisplayType
		{
			get { return PhotoType.ToString(); }
		}

		public override byte[] GetImagePreview()
		{
			return File.ReadAllBytes(file.FullName);
		}
	}

	public class ExtractAction : EmbedExtractAction
	{
		private int index;
		public string dst;
		public ExtractAction(Song sourceSong, int index, string destinationFilename = null)
		{
			this.AssociatedSong = sourceSong;
			this.index = index;
			this.dst = destinationFilename;
			if (String.IsNullOrEmpty(this.dst))
			{
				var ext = "jpg";
				var mime = AssociatedSong.mp3.Tag.Pictures[index].MimeType.ToLower();
				if (mime == "image/gif")
					ext = "gif";
				else if (mime == "image/png")
					ext = "png";
				else if (mime == "image/bmp")
					ext = "bmp";

				this.dst = Path.Combine(AssociatedSong.File.Directory.FullName, "folder." + ext);
			}
			ExecuteOrder = 2;
		}

		public override void ExecuteAction()
		{
			File.WriteAllBytes(dst, AssociatedSong.mp3.Tag.Pictures[index].Data.ToArray());
		}

		public override string DisplayAction
		{
			get { return "Extract"; }
		}

		public override string DisplayDirectory
		{
			get { return AssociatedSong.File.Directory.FullName; }
		}

		public override string DisplaySource
		{
			get { return String.Format("<tag:{0}> ", index) + AssociatedSong.File.Name; }
		}

		public override string DisplayTarget
		{
			get { return dst; }
		}

		public override string DisplayType
		{
			get { return string.Empty; }
		}

		public override byte[] GetImagePreview()
		{
			return AssociatedSong.mp3.Tag.Pictures[index].Data.ToArray();
		}
	}

	public class DeleteFileAction : EmbedExtractAction
	{
		public FileInfo f;
		public DeleteFileAction(Song AssociatedSong, FileInfo file)
		{
			this.AssociatedSong = AssociatedSong;
			f = file;
			ExecuteOrder = 3;
		}

		public override void ExecuteAction()
		{
			DeleteToRecycleBin.RecycleBin.SendSilent(f.FullName);
		}

		public override string DisplayAction
		{
			get { return "Delete File"; }
		}

		public override string DisplayDirectory
		{
			get { return f.Directory.FullName; }
		}

		public override string DisplaySource
		{
			get { return f.Name; }
		}

		public override string DisplayTarget
		{
			get { return "{recycle bin}"; }
		}

		public override string DisplayType
		{
			get { return string.Empty; }
		}

		public override byte[] GetImagePreview()
		{
			return File.ReadAllBytes(f.FullName);
		}
	}

	public class DeleteEmbeddedAction : EmbedExtractAction
	{
		private int index;
		public DeleteEmbeddedAction(Song sourceSong, int index)
		{
			this.AssociatedSong = sourceSong;
			this.index = index;
			ExecuteOrder = 4;
		}

		public override void ExecuteAction()
		{
			var x = new List<TagLib.IPicture>(AssociatedSong.mp3.Tag.Pictures);
			x.RemoveAt(index);
			AssociatedSong.mp3.Tag.Pictures = x.ToArray();
			AssociatedSong.Dirty = true;
		}

		public override string DisplayAction
		{
			get { return "Delete Embedded"; }
		}

		public override string DisplayDirectory
		{
			get { return AssociatedSong.File.Directory.FullName; }
		}

		public override string DisplaySource
		{
			get { return String.Format("<tag:{0}> ", index) + AssociatedSong.File.Name; }
		}

		public override string DisplayTarget
		{
			get { return "{permanently delete}"; }
		}

		public override string DisplayType
		{
			get { return string.Empty; }
		}

		public override byte[] GetImagePreview()
		{
			return AssociatedSong.mp3.Tag.Pictures[index].Data.ToArray();
		}
	}
}
