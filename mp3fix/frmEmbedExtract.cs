﻿using Delimon.Win32.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mp3fix
{
	public partial class frmEmbedExtract : Form
	{
		public frmEmbedExtract()
		{
			InitializeComponent();
		}

		public List<EmbedExtractAction> Actions = new List<EmbedExtractAction>();
		private List<EmbedExtractAction> ActionView;
		

		private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
		{
			listView1.Refresh();
		}

		private void frmEmbedExtract_Load(object sender, EventArgs e)
		{

		}

		public void UpdateView(List<EmbedExtractAction> lst)
		{
			try
			{
				ActionView = new List<EmbedExtractAction>(lst);
				listView1.BeginUpdate();
				listView1.VirtualMode = true;
				listView1.VirtualListSize = ActionView.Count;
				listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
			}
			finally
			{
				listView1.EndUpdate();
			}
		}

		private void listView1_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
		{
			e.Item = ActionView[e.ItemIndex].GetListItem();
		}

		Regex guidX;
		
		bool noprefchange = false;
		private void prefchange(object sender, EventArgs e)
		{
			if (noprefchange)
				return;
			try
			{
				guidX = null;
				textBox1_TextChanged(sender, e);

				noprefchange = true;
				listView1.BeginUpdate();
				propertyGrid1.Enabled = false;
				ck_deleteEmb.Enabled = true;
				ck_deleteguid.Enabled = true;
				ck_embedempty.Enabled = true;
				ck_extractempty.Enabled = true;

				if (mp_embed.Checked)
				{
					foreach (var f in Actions)
						if (f is EmbedAction || f is DeleteFileAction)
							f.Enabled = true;
						else
							f.Enabled = false;

				}
				else if (mp_extract.Checked)
				{
					ck_deleteEmb.CheckState = CheckState.Checked;
					ck_deleteEmb.Enabled = false;
					ck_embedempty.CheckState = CheckState.Unchecked;
					ck_embedempty.Enabled = false;

					foreach (var f in Actions)
						if (f is ExtractAction || f is DeleteEmbeddedAction)
							f.Enabled = true;
						else
							f.Enabled = false;

				}
				else if (mp_both.Checked)
				{
					ck_deleteEmb.CheckState = CheckState.Unchecked;
					ck_deleteEmb.Enabled = false;

					foreach (var f in Actions)
						if (f is DeleteEmbeddedAction || f is DeleteFileAction)
							f.Enabled = false;
						else
							f.Enabled = true;
				}
				else if (mp_custom.Checked)
				{
					propertyGrid1.Enabled = true;
				}

				if (ck_deleteEmb.CheckState == CheckState.Checked)
					foreach (var f in Actions)
						if (f is DeleteEmbeddedAction)
							f.Enabled = true;

				if (ck_deleteguid.CheckState == CheckState.Checked && guidX != null)
				{
					foreach (var f in Actions)
						if (f is DeleteFileAction
							&& guidX.IsMatch((f as DeleteFileAction).f.Name))
							f.Enabled = true;
						else if (f is EmbedAction
							&& guidX.IsMatch((f as EmbedAction).file.Name))
							f.Enabled = false;
				}

				if (ck_embedempty.CheckState == CheckState.Checked) // embed only if song is empty
				{
					foreach (var f in Actions)
						if (f is EmbedAction &&
							f.AssociatedSong.mp3.Tag.Pictures.Length > 0) // song not empty
							f.Enabled = false;
				}

				if (ck_extractempty.CheckState == CheckState.Checked) // no overwrite on extract
				{
					foreach (var f in Actions)
						if (f is ExtractAction &&
							File.Exists((f as ExtractAction).dst)) // file exists
							f.Enabled = false;
				}

			}
			catch (Exception x)
			{

			}
			finally
			{
				List<EmbedExtractAction> axs = new List<EmbedExtractAction>(Actions);
				if (!mp_custom.Checked)
					axs.RemoveAll(f => !f.Enabled);
				UpdateView(axs);
				listView1.EndUpdate();
				noprefchange = false;
			}
		}

		private void listView1_SelectedIndexChanged(object sender, EventArgs e)
		{
			List<EmbedExtractAction> ee = new List<EmbedExtractAction>();
			foreach (int index in listView1.SelectedIndices)
				ee.Add(ActionView[index]);
			propertyGrid1.SelectedObjects = ee.ToArray();

			if (ee.Count > 0)
			{
				try
				{
					byte[] b = ee.First().GetImagePreview();
					using (var ms = new System.IO.MemoryStream(b))
					{
						var bmp = Bitmap.FromStream(ms);
						pictureBox1.Image = bmp;
						label1.Text = string.Format("{0}x{1}", bmp.Width, bmp.Height);
					}
				}
				catch
				{ }
			}
		}

		private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
		{
			try
			{
				listView1.BeginUpdate();
				switch (e.Column)
				{
					case 0:
						ActionView = ActionView.OrderBy(f => f.ExecuteOrder).ToList();
						break;
					case 1:
						ActionView = ActionView.OrderBy(f => f.DisplayDirectory).ToList();
						break;
					case 2:
						ActionView = ActionView.OrderBy(f => f.DisplaySource).ToList();
						break;
					case 3:
						ActionView = ActionView.OrderBy(f => f.DisplayTarget).ToList();
						break;
					case 4:
						ActionView = ActionView.OrderBy(f => f.DisplayType).ToList();
						break;
				}
			}
			finally
			{
				listView1.EndUpdate();
			}
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
			try
			{
				guidX = new Regex(textBox1.Text, RegexOptions.IgnoreCase);
				textBox1.ForeColor = this.ForeColor;
			}
			catch
			{
				textBox1.ForeColor = System.Drawing.Color.Red;
			}
		}
	}
}
