﻿namespace mp3fix
{
	partial class frmEmbedExtract
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listView1 = new System.Windows.Forms.ListView();
			this.ch_action = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.ch_dir = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.ch_src = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.ch_dst = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.ch_data = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.ck_extractempty = new System.Windows.Forms.CheckBox();
			this.ck_deleteguid = new System.Windows.Forms.CheckBox();
			this.ck_embedempty = new System.Windows.Forms.CheckBox();
			this.ck_deleteEmb = new System.Windows.Forms.CheckBox();
			this.mp_custom = new System.Windows.Forms.RadioButton();
			this.mp_both = new System.Windows.Forms.RadioButton();
			this.mp_extract = new System.Windows.Forms.RadioButton();
			this.mp_embed = new System.Windows.Forms.RadioButton();
			this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
			this.label1 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// listView1
			// 
			this.listView1.AllowColumnReorder = true;
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ch_action,
            this.ch_dir,
            this.ch_src,
            this.ch_dst,
            this.ch_data});
			this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listView1.Location = new System.Drawing.Point(0, 0);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(494, 585);
			this.listView1.TabIndex = 0;
			this.listView1.UseCompatibleStateImageBehavior = false;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.VirtualMode = true;
			this.listView1.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listView1_ColumnClick);
			this.listView1.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.listView1_RetrieveVirtualItem);
			this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
			// 
			// ch_action
			// 
			this.ch_action.Text = "Action";
			// 
			// ch_dir
			// 
			this.ch_dir.Text = "Directory";
			// 
			// ch_src
			// 
			this.ch_src.Text = "Source";
			// 
			// ch_dst
			// 
			this.ch_dst.Text = "Target";
			// 
			// ch_data
			// 
			this.ch_data.Text = "Type";
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.listView1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.textBox1);
			this.splitContainer1.Panel2.Controls.Add(this.ck_extractempty);
			this.splitContainer1.Panel2.Controls.Add(this.ck_deleteguid);
			this.splitContainer1.Panel2.Controls.Add(this.ck_embedempty);
			this.splitContainer1.Panel2.Controls.Add(this.ck_deleteEmb);
			this.splitContainer1.Panel2.Controls.Add(this.mp_custom);
			this.splitContainer1.Panel2.Controls.Add(this.mp_both);
			this.splitContainer1.Panel2.Controls.Add(this.mp_extract);
			this.splitContainer1.Panel2.Controls.Add(this.mp_embed);
			this.splitContainer1.Panel2.Controls.Add(this.propertyGrid1);
			this.splitContainer1.Panel2.Controls.Add(this.label1);
			this.splitContainer1.Panel2.Controls.Add(this.pictureBox1);
			this.splitContainer1.Panel2.Controls.Add(this.button2);
			this.splitContainer1.Panel2.Controls.Add(this.button1);
			this.splitContainer1.Size = new System.Drawing.Size(754, 585);
			this.splitContainer1.SplitterDistance = 494;
			this.splitContainer1.TabIndex = 10;
			// 
			// textBox1
			// 
			this.textBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.textBox1.Location = new System.Drawing.Point(36, 323);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(192, 20);
			this.textBox1.TabIndex = 21;
			this.textBox1.Text = "........-....-....-....-............|[0-9A-F]{32}|AlbumArtSmall\\.jpg";
			this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// ck_extractempty
			// 
			this.ck_extractempty.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.ck_extractempty.AutoSize = true;
			this.ck_extractempty.Location = new System.Drawing.Point(18, 278);
			this.ck_extractempty.Name = "ck_extractempty";
			this.ck_extractempty.Size = new System.Drawing.Size(202, 17);
			this.ck_extractempty.TabIndex = 20;
			this.ck_extractempty.Text = "Skip extract if target file already exists";
			this.ck_extractempty.UseVisualStyleBackColor = true;
			this.ck_extractempty.CheckedChanged += new System.EventHandler(this.prefchange);
			// 
			// ck_deleteguid
			// 
			this.ck_deleteguid.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.ck_deleteguid.AutoSize = true;
			this.ck_deleteguid.Location = new System.Drawing.Point(18, 300);
			this.ck_deleteguid.Name = "ck_deleteguid";
			this.ck_deleteguid.Size = new System.Drawing.Size(203, 17);
			this.ck_deleteguid.TabIndex = 19;
			this.ck_deleteguid.Text = "Skip and delete files that match regex";
			this.ck_deleteguid.UseVisualStyleBackColor = true;
			this.ck_deleteguid.CheckedChanged += new System.EventHandler(this.prefchange);
			// 
			// ck_embedempty
			// 
			this.ck_embedempty.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.ck_embedempty.AutoSize = true;
			this.ck_embedempty.Location = new System.Drawing.Point(18, 256);
			this.ck_embedempty.Name = "ck_embedempty";
			this.ck_embedempty.Size = new System.Drawing.Size(228, 17);
			this.ck_embedempty.TabIndex = 18;
			this.ck_embedempty.Text = "Embed only in songs with no embedded art";
			this.ck_embedempty.UseVisualStyleBackColor = true;
			this.ck_embedempty.CheckedChanged += new System.EventHandler(this.prefchange);
			// 
			// ck_deleteEmb
			// 
			this.ck_deleteEmb.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.ck_deleteEmb.AutoSize = true;
			this.ck_deleteEmb.Location = new System.Drawing.Point(18, 234);
			this.ck_deleteEmb.Name = "ck_deleteEmb";
			this.ck_deleteEmb.Size = new System.Drawing.Size(148, 17);
			this.ck_deleteEmb.TabIndex = 17;
			this.ck_deleteEmb.Text = "Delete existing embedded";
			this.ck_deleteEmb.UseVisualStyleBackColor = true;
			this.ck_deleteEmb.CheckedChanged += new System.EventHandler(this.prefchange);
			this.ck_deleteEmb.CheckStateChanged += new System.EventHandler(this.prefchange);
			// 
			// mp_custom
			// 
			this.mp_custom.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.mp_custom.AutoSize = true;
			this.mp_custom.Checked = true;
			this.mp_custom.Location = new System.Drawing.Point(18, 349);
			this.mp_custom.Name = "mp_custom";
			this.mp_custom.Size = new System.Drawing.Size(60, 17);
			this.mp_custom.TabIndex = 16;
			this.mp_custom.TabStop = true;
			this.mp_custom.Text = "Custom";
			this.mp_custom.UseVisualStyleBackColor = true;
			this.mp_custom.CheckedChanged += new System.EventHandler(this.prefchange);
			// 
			// mp_both
			// 
			this.mp_both.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.mp_both.AutoSize = true;
			this.mp_both.Enabled = false;
			this.mp_both.Location = new System.Drawing.Point(18, 202);
			this.mp_both.Name = "mp_both";
			this.mp_both.Size = new System.Drawing.Size(145, 17);
			this.mp_both.TabIndex = 15;
			this.mp_both.Text = "Embed and extract (sync)";
			this.mp_both.UseVisualStyleBackColor = true;
			this.mp_both.CheckedChanged += new System.EventHandler(this.prefchange);
			// 
			// mp_extract
			// 
			this.mp_extract.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.mp_extract.AutoSize = true;
			this.mp_extract.Location = new System.Drawing.Point(18, 180);
			this.mp_extract.Name = "mp_extract";
			this.mp_extract.Size = new System.Drawing.Size(129, 17);
			this.mp_extract.TabIndex = 14;
			this.mp_extract.Text = "Extract and un-embed";
			this.mp_extract.UseVisualStyleBackColor = true;
			this.mp_extract.CheckedChanged += new System.EventHandler(this.prefchange);
			// 
			// mp_embed
			// 
			this.mp_embed.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.mp_embed.AutoSize = true;
			this.mp_embed.Location = new System.Drawing.Point(18, 158);
			this.mp_embed.Name = "mp_embed";
			this.mp_embed.Size = new System.Drawing.Size(175, 17);
			this.mp_embed.TabIndex = 13;
			this.mp_embed.Text = "Embed and delete loose images";
			this.mp_embed.UseVisualStyleBackColor = true;
			this.mp_embed.CheckedChanged += new System.EventHandler(this.prefchange);
			// 
			// propertyGrid1
			// 
			this.propertyGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.propertyGrid1.HelpVisible = false;
			this.propertyGrid1.Location = new System.Drawing.Point(3, 372);
			this.propertyGrid1.Name = "propertyGrid1";
			this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
			this.propertyGrid1.Size = new System.Drawing.Size(250, 146);
			this.propertyGrid1.TabIndex = 12;
			this.propertyGrid1.ToolbarVisible = false;
			this.propertyGrid1.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid1_PropertyValueChanged);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.ForeColor = System.Drawing.SystemColors.GrayText;
			this.label1.Location = new System.Drawing.Point(0, 132);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(250, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "0x0";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlDark;
			this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pictureBox1.Location = new System.Drawing.Point(3, 3);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(250, 126);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 11;
			this.pictureBox1.TabStop = false;
			// 
			// button2
			// 
			this.button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.Location = new System.Drawing.Point(135, 535);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(93, 28);
			this.button2.TabIndex = 9;
			this.button2.Text = "Ca&ncel";
			this.button2.UseVisualStyleBackColor = true;
			// 
			// button1
			// 
			this.button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.button1.Location = new System.Drawing.Point(36, 535);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(93, 28);
			this.button1.TabIndex = 8;
			this.button1.Text = "O&K";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// frmEmbedExtract
			// 
			this.AcceptButton = this.button1;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.button2;
			this.ClientSize = new System.Drawing.Size(754, 585);
			this.Controls.Add(this.splitContainer1);
			this.MinimumSize = new System.Drawing.Size(483, 290);
			this.Name = "frmEmbedExtract";
			this.Text = "Embed and Extract Album Art";
			this.Load += new System.EventHandler(this.frmEmbedExtract_Load);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.ColumnHeader ch_action;
		private System.Windows.Forms.ColumnHeader ch_src;
		private System.Windows.Forms.ColumnHeader ch_dst;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.PropertyGrid propertyGrid1;
		private System.Windows.Forms.ColumnHeader ch_data;
		private System.Windows.Forms.ColumnHeader ch_dir;
		private System.Windows.Forms.RadioButton mp_both;
		private System.Windows.Forms.RadioButton mp_extract;
		private System.Windows.Forms.RadioButton mp_embed;
		private System.Windows.Forms.RadioButton mp_custom;
		private System.Windows.Forms.CheckBox ck_deleteEmb;
		private System.Windows.Forms.CheckBox ck_extractempty;
		private System.Windows.Forms.CheckBox ck_deleteguid;
		private System.Windows.Forms.CheckBox ck_embedempty;
		private System.Windows.Forms.TextBox textBox1;
	}
}