﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mp3fix
{
	public partial class frmLoad : Form
	{
		public frmLoad()
		{
			InitializeComponent();
		}

		public void SetDirectory(string text)
		{
			if (this.lbl_cd.InvokeRequired)
				this.lbl_cd.BeginInvoke(new MethodInvoker(delegate() { SetDirectory(text); }));
			else
				this.lbl_cd.Text = text;
			Application.DoEvents();
		}

		private void frmLoad_Load(object sender, EventArgs e)
		{

		}
	}
}
