﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Delimon.Win32.IO;
using System.Text.RegularExpressions;

namespace mp3fix
{

	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			folderSelect.InitialDirectory = @"D:\Music\@todo-v2";
		}

		SongCollection songs = new SongCollection();

		private void Form1_Load(object sender, EventArgs e)
		{
		}

		FolderSelect.FolderSelectDialog folderSelect = new FolderSelect.FolderSelectDialog();

		private void button3_Click(object sender, EventArgs e)
		{
			String dir;
			if (folderSelect.ShowDialog())
				dir = folderSelect.FileName;
			else
				return;

			songs = new SongCollection();
			songs.LoadDirInteractive(dir);

			try
			{
				listView1.BeginUpdate();
				songs.UpdateArtConsistency();
				listView1.VirtualListSize = songs.Count;
				listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
			}
			finally
			{
				listView1.EndUpdate();
			}
			try
			{
				lstCleanup.BeginUpdate();
				lstCleanup.Items.Clear();
				foreach (var x in songs.Cleanup_EmptyDirs)
					lstCleanup.Items.Add(new ListViewItem(x.FullName)).SubItems.Add("Directory is empty or contains only empty subfolders");
				foreach (var x in songs.Cleanup_ContainsOnlyArt)
					lstCleanup.Items.Add(new ListViewItem(x.FullName)).SubItems.Add("Directory contains only album art");
				foreach (var x in songs.Cleanup_ContainsOnlySys)
					lstCleanup.Items.Add(new ListViewItem(x.FullName)).SubItems.Add("Directory contains only system files");
				foreach (var x in songs.Cleanup_SysFiles)
					lstCleanup.Items.Add(new ListViewItem(x.FullName)).SubItems.Add("System file");
				foreach (var x in songs.Cleanup_CorruptMusic)
					lstCleanup.Items.Add(new ListViewItem(x.FullName)).SubItems.Add("Corrupt file");
				lstCleanup.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
			}
			finally
			{
				lstCleanup.EndUpdate();
			} 
		}

		private void listView1_SearchForVirtualItem(object sender, SearchForVirtualItemEventArgs e)
		{
			
		}

		private void listView1_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
		{
			e.Item = songs.Songs[e.ItemIndex].GetItem();
		}

		private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
		{
			switch (e.Column)
			{
				case 0:
					songs.Songs = songs.Songs.OrderBy(f => f.File.FullName).ToList();
					break;
				case 1:
					songs.Songs = songs.Songs.OrderBy(f => f.Title).ToList();
					break;
				case 2:
					songs.Songs = songs.Songs.OrderBy(f => f.mp3.Tag.FirstAlbumArtist).ToList();
					break;
				case 3:
					songs.Songs = songs.Songs.OrderBy(f => f.mp3.Tag.FirstPerformer).ToList();
					break;
				case 4:
					songs.Songs = songs.Songs.OrderBy(f => f.mp3.Tag.Album).ToList();
					break;
				case 5:
					songs.Songs = songs.Songs.OrderBy(f => f.mp3.Tag.Pictures.Length).ToList();
					break;
				case 6:
					songs.Songs = songs.Songs.OrderBy(f => f.AccompanyingFiles.Count).ToList();
					break;
				case 7:
					songs.Songs = songs.Songs.OrderBy(f => f.AlbumArtConsistent.ToString()).ToList();
					break;
				default:
					break;
			}
			(sender as ListView).Refresh();
		}

		private void listView1_ItemDrag(object sender, ItemDragEventArgs e)
		{
			var items = new List<string>();
			foreach (int x in listView1.SelectedIndices)
				items.Add(songs.Songs[x].File.FullName);

			var l = items.ToArray();
			DataObject data = new DataObject();
			data.SetData(DataFormats.FileDrop, l);
			data.SetData("FileName", l[0]);
			data.SetData("FileNameW", l[0]);
			listView1.DoDragDrop(data, DragDropEffects.Copy);
		}

		private void listView1_DragDrop(object sender, DragEventArgs e)
		{
			e.Effect = DragDropEffects.Copy;
		}

		private void listView1_DragEnter(object sender, DragEventArgs e)
		{
			e.Effect = DragDropEffects.Copy;
		}

		private void btnTrim_Click(object sender, EventArgs e)
		{

		}

		private static void Trim(string[] strings)
		{
			if (strings != null)
				for (int i = 0; i < strings.Length; ++i)
					strings[i] = strings[i].Trim();
		}

		private void button2_Click(object sender, EventArgs e)
		{

		}

		private void button8_Click(object sender, EventArgs e)
		{
			try
			{
				lstCleanup.BeginUpdate();
				foreach (ListViewItem x in lstCleanup.SelectedItems)
				{
					if (x.ForeColor != System.Drawing.SystemColors.GrayText)
					{
						try
						{
							//Microsoft.VisualBasic.FileIO.FileSystem.DeleteFile(x.Text, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin, Microsoft.VisualBasic.FileIO.UICancelOption.ThrowException);

							if (DeleteToRecycleBin.RecycleBin.Send(x.Text))
							{
								x.Font = new Font(x.Font, FontStyle.Strikeout);
								x.ForeColor = System.Drawing.SystemColors.GrayText;
							}

						}
						catch (Exception ex)
						{
							x.SubItems[1].Text = ex.ToString();
						}
					}
				}
			}
			finally
			{
				lstCleanup.EndUpdate();
			}
		}

		private void lstCleanup_ItemActivate(object sender, EventArgs e)
		{
			if (lstCleanup.SelectedItems.Count > 0)
			{
				var d = lstCleanup.SelectedItems[0].Text;
				if (Directory.Exists(d))
					System.Diagnostics.Process.Start(d);
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			using (frmEmbedExtract ee = new frmEmbedExtract())
			{
				HashSet<string> deleteFiles = new HashSet<string>();
				foreach (int index in listView1.SelectedIndices)
				{
					var song = songs.Songs[index];
					var files = song.AccompanyingFiles;

					//possible: embed files in song 
					foreach (var file in files)
						ee.Actions.Add(new EmbedAction(file, song));

					//possible: extract files from song
					for (int i = 0; i < song.mp3.Tag.Pictures.Length; ++i)
						ee.Actions.Add(new ExtractAction(song, i));

					//possible: delete files
					foreach (var file in files)
						//ee.Actions.Add(new DeleteFileAction(song, file));
						deleteFiles.Add(file.FullName);

					//possible: delete embedded files
					for (int i = song.mp3.Tag.Pictures.Length - 1; i >= 0;  --i)
						ee.Actions.Add(new DeleteEmbeddedAction(song, i));
				}

				foreach (var file in deleteFiles)
					ee.Actions.Add(new DeleteFileAction(null, new FileInfo(file)));

				for (int i = 0; i < ee.Actions.Count; ++i)
					ee.Actions[i].CreationOrder = i;

				ee.Actions = (from x in ee.Actions
							  orderby x.ExecuteOrder
							  select x).ToList();

				ee.UpdateView(ee.Actions);
				if (ee.ShowDialog() == System.Windows.Forms.DialogResult.OK)
				{
					// reset
					foreach (var song in ee.Actions
							.Select(x => x.AssociatedSong)
							.Where(x => x != null)
							.Distinct())
					{
						song.OriginalTag.CopyTo(song.mp3.Tag, true); // reset before applying
					}

					// apply actions
					ee.Actions = ee.Actions.OrderBy(f => f.CreationOrder).ToList(); // put back in order
					for (int i = 0; i < ee.Actions.Count; ++i)
						if (ee.Actions[i].Enabled)
							ee.Actions[i].ExecuteAction();

					// save files that had actions executed on them
					foreach (var song in ee.Actions
							.Select(x => x.AssociatedSong)
							.Where(x => x != null && x.Dirty)
							.Distinct())
					{
						song.mp3.Save();
						song.mp3.Tag.CopyTo(song.OriginalTag, true);
						song.Dirty = false;
					}

					// update art file list
					listView1.BeginUpdate();
					songs.ArtFiles = songs.ArtFiles.Where(a => a.Exists).ToList();
					songs.UpdateArtConsistency();
					updatechk(sender, e);
					listView1.EndUpdate();
				}
			}
		}

		private void button2_Click_1(object sender, EventArgs e)
		{
			foreach (int x in listView1.SelectedIndices)
			{
				songs.Songs[x].mp3.Save();
				songs.Songs[x].mp3.Tag.CopyTo(songs.Songs[x].OriginalTag, true);
				songs.Songs[x].Invalidate();
			}
			listView1.Refresh();
		}

		private void updatechk(object sender, EventArgs e)
		{
			try
			{
				listView1.BeginUpdate();

				foreach (var song in songs.Songs)
				{
					song.OriginalTag.CopyTo(song.mp3.Tag, true);
					song.Invalidate();
				}

				if (ck_variousartists.Checked)
				{
					foreach (var song in songs.Songs.Where(s => s.mp3.Tag.Performers.Length > 1 || s.mp3.Tag.JoinedAlbumArtists == "Various"))
					{
						song.mp3.Tag.AlbumArtists = new string[] { "Various Artists" };
						song.Invalidate();
					}
				}

				if (ck_artistsort.Checked)
				{
					foreach (var song in songs.Songs.Where(s => s.mp3.Tag.AlbumArtists.Length > 1 || s.mp3.Tag.Performers.Length > 1))
					{
						Array.Sort(song.mp3.Tag.AlbumArtists);
						Array.Sort(song.mp3.Tag.Performers);
						song.Invalidate();
					}
				}

				if (ck_genresort.Checked)
				{
					foreach (var song in songs.Songs.Where(s => s.mp3.Tag.Genres.Length > 1 ))
					{
						Array.Sort(song.mp3.Tag.Genres);
						song.Invalidate();
					}
				}

				if (ck_genrecase.Checked)
				{
					foreach (var song in songs.Songs.Where(s => s.mp3.Tag.Genres.Length > 0))
					{
						for (int i = 0; i < song.mp3.Tag.Genres.Length; ++i)
						{
							var sc = song.mp3.Tag.Genres[i].ToSentenceCase();
							if (sc != song.mp3.Tag.Genres[i])
							{
								song.mp3.Tag.Genres[i] = sc;
								song.Dirty = true;
							}
						}
					}
				}

				if (ck_trim.Checked)
				{
					foreach (var song in songs.Songs)
					{
						Regex rs = new Regex(@"\s{2,}");
						song.mp3.Tag.Album = rs.Replace(song.mp3.Tag.Album ?? "", " ").Trim();
						song.mp3.Tag.Title = rs.Replace(song.mp3.Tag.Title ?? "", " ").Trim();

						for (int i = 0; i < song.mp3.Tag.Performers.Length; ++i)
							song.mp3.Tag.Performers[i] = rs.Replace(song.mp3.Tag.Performers[i] ??"", "").Trim();
		
						for (int i = 0; i < song.mp3.Tag.AlbumArtists.Length; ++i)
							song.mp3.Tag.AlbumArtists[i] = rs.Replace(song.mp3.Tag.AlbumArtists[i] ??"", "").Trim();
					}
				}

				if (ck_unifyamp.Checked)
					UnifyAmpersands();

				if (ck_unifycase.Checked)
					UnifyCases();

				for (int x = 0; x < songs.Songs.Count; ++x)
				{
					var mp3 = songs.Songs[x].mp3;

					if (ck_trim.Checked)
					{
						if (mp3.Tag.Title != null) mp3.Tag.Title = mp3.Tag.Title.Trim();
						if (mp3.Tag.Album != null) mp3.Tag.Album = mp3.Tag.Album.Trim();
						if (mp3.Tag.Conductor != null) mp3.Tag.Conductor = mp3.Tag.Conductor.Trim();
						if (mp3.Tag.Copyright != null) mp3.Tag.Copyright = mp3.Tag.Copyright.Trim();
						if (mp3.Tag.Comment != null) mp3.Tag.Comment = mp3.Tag.Comment.Trim();
						Trim(mp3.Tag.AlbumArtists);
						Trim(mp3.Tag.Composers);
						Trim(mp3.Tag.Performers);
						Trim(mp3.Tag.Genres);
					}

					if (ck_fillemptyaa.Checked &&
						String.IsNullOrWhiteSpace(mp3.Tag.JoinedAlbumArtists) &&
						!String.IsNullOrWhiteSpace(mp3.Tag.JoinedPerformers))
						mp3.Tag.AlbumArtists = mp3.Tag.Performers;

					else if (
						ck_unifycase.Checked &&
						!String.IsNullOrWhiteSpace(mp3.Tag.JoinedAlbumArtists) && !String.IsNullOrWhiteSpace(mp3.Tag.JoinedPerformers)
						&& !mp3.Tag.JoinedAlbumArtists.Equals(mp3.Tag.JoinedPerformers)
						&& mp3.Tag.JoinedAlbumArtists.Equals(mp3.Tag.JoinedPerformers, StringComparison.OrdinalIgnoreCase))
						mp3.Tag.AlbumArtists = mp3.Tag.Performers;

					songs.Songs[x].Invalidate();
				}


			}
			finally
			{
				listView1.EndUpdate();
			}
		}

		private void UnifyAmpersands()
		{
			// album
			{
				var query = from x in songs.Songs
							where x.mp3.Tag.Album != null
							let m = x.mp3.Tag.Album.ToLower().Replace("&", "").Replace("and", "").Replace(" ", "")
							group x by m into grp
							where grp.Select(y => y.mp3.Tag.Album).Distinct().Count() > 1
							select grp;

				foreach (var group in query)
				{
					var master = group.First().mp3.Tag.Album;
					foreach (var slave in group.Skip(1))
						slave.mp3.Tag.Album = master;
				}
			}

			// performer
			{
				var query = from x in songs.Songs
							where x.mp3.Tag.Performers != null
							let m = x.mp3.Tag.JoinedPerformers.ToLower().Replace("&", "").Replace("and", "").Replace(" ", "")
							group x by m into grp
							where grp.Select(y => y.mp3.Tag.JoinedPerformers).Distinct().Count() > 1
							select grp;

				foreach (var group in query)
				{
					var master = group.First().mp3.Tag.Performers;
					foreach (var slave in group.Skip(1))
						slave.mp3.Tag.Performers = master;
				}
			}

			// album-artist
			{
				var query = from x in songs.Songs
							where x.mp3.Tag.AlbumArtists != null
							let m = x.mp3.Tag.JoinedAlbumArtists.ToLower().Replace("&", "").Replace("and", "").Replace(" ", "")
							group x by m into grp
							where grp.Select(y => y.mp3.Tag.JoinedAlbumArtists).Distinct().Count() > 1
							select grp;

				foreach (var group in query)
				{
					var master = group.First().mp3.Tag.AlbumArtists;
					foreach (var slave in group.Skip(1))
						slave.mp3.Tag.AlbumArtists = master;
				}
			}

			// performer to album-artist (across)
			{
				var query = from x in songs.Songs
							where x.mp3.Tag.Performers != null
							let m = x.mp3.Tag.JoinedPerformers.ToLower().Replace("&", "").Replace("and", "").Replace(" ", "")
							let n = x.mp3.Tag.JoinedAlbumArtists.ToLower().Replace("&", "").Replace("and", "").Replace(" ", "")
							where m == n
							select x;

				foreach (var song in query)
					song.mp3.Tag.AlbumArtists = song.mp3.Tag.Performers;
			}
		}


		private void UnifyCases()
		{
			// album
			{
				var query = from x in songs.Songs
							let p = x.mp3.Tag.Album 
							where p != null
							group x by p.ToLower() into grp
							where grp.Select(y => y.mp3.Tag.Album).Distinct().Count() > 1
							select grp;

				foreach (var group in query)
				{
					var master = group.First().mp3.Tag.Album;
					foreach (var slave in group.Skip(1))
						slave.mp3.Tag.Album = master;
				}
			}

			// performer
			{
				var query = from x in songs.Songs
							let p = x.mp3.Tag.JoinedPerformers
							where p != null
							group x by p.ToLower() into grp
							where grp.Select(y => y.mp3.Tag.JoinedPerformers).Distinct().Count() > 1
							select grp;

				foreach (var group in query)
				{
					var master = group.First().mp3.Tag.Performers;
					foreach (var slave in group.Skip(1))
						slave.mp3.Tag.Performers = master;
				}
			}

			// album-artist
			{
				var query = from x in songs.Songs
							let p = x.mp3.Tag.JoinedAlbumArtists
							where p != null
							group x by p.ToLower() into grp
							where grp.Select(y => y.mp3.Tag.JoinedAlbumArtists).Distinct().Count() > 1
							select grp;

				foreach (var group in query)
				{
					var master = group.First().mp3.Tag.AlbumArtists;
					foreach (var slave in group.Skip(1))
						slave.mp3.Tag.AlbumArtists = master;
				}
			}
		}

	}

	public class SongCollection
	{
		public List<Song> Songs = new List<Song>();
		public List<FileInfo> ArtFiles = new List<FileInfo>();
		public List<DirectoryInfo> Cleanup_EmptyDirs = new List<DirectoryInfo>();
		public List<DirectoryInfo> Cleanup_ContainsOnlySys = new List<DirectoryInfo>();
		public List<DirectoryInfo> Cleanup_ContainsOnlyArt = new List<DirectoryInfo>();
		public List<FileInfo> Cleanup_SysFiles = new List<FileInfo>();
		public List<FileInfo> Cleanup_CorruptMusic = new List<FileInfo>();

		private System.Threading.CancellationTokenSource cts = new System.Threading.CancellationTokenSource();
		
		public IEnumerable<Task> ActiveLoadTasks
		{
			get
			{
				return from x in Songs
					   where x.loadTask != null && !x.loadTask.IsCompleted
					   select x.loadTask;
			}
		}

		public void AbortLoad()
		{

			foreach (var x in ActiveLoadTasks)
				x.Wait();
		}

		public bool IsLoading()
		{
			foreach (var x in ActiveLoadTasks)
				return true;
			return false;
		}

		public void LoadDirInteractive(string dir)
		{
			LoadDirInteractive(new DirectoryInfo(dir));
		}

		private static readonly Regex SystemFileRegex = new Regex("\\A(?:\\.(ini|url|nfo)$|^thumbs\\.db$)\\z", RegexOptions.IgnoreCase);

		public void LoadDirInteractive(DirectoryInfo dir)
		{
			using (frmLoad l = new frmLoad())
			{
				l.Show();
				Task t = new Task(() => { LoadDirInternal(l, dir); });
				t.Start();
				t.Wait();
				l.SetDirectory("Finishing up");
				foreach (var x in ActiveLoadTasks)
					x.Wait();
				UpdateArtConsistency();
				l.Hide();
			}
		}

		private void LoadDirInternal(frmLoad l, DirectoryInfo dir)
		{
			l.SetDirectory(dir.FullName);

			var dd = dir.GetDirectories();
			var df = dir.GetFiles();
			if (dd.Length > 0)
			{
				foreach (var di in dd)
					LoadDirInternal(l, di);
			}
			else if (df.Length > 0)
			{
				var mp3files = df.Where(f => f.Name.ToLower().EndsWith(".mp3")).ToArray();
				var jpgFiles = df.Where(f => f.Name.ToLower().EndsWith(".jpg")).ToArray();
				var sysFiles = df.Where(f => SystemFileRegex.IsMatch(f.Name));

				foreach (var fi in mp3files)
					Songs.Add(new Song(this, fi, cts.Token));
				foreach (var fi in jpgFiles)
					ArtFiles.Add(fi);
				foreach (var fi in sysFiles)
					Cleanup_SysFiles.Add(fi);

				if (df.Except(sysFiles).Count() == 0)
					Cleanup_ContainsOnlySys.Add(dir);
				else if (df.Except(jpgFiles).Except(sysFiles).Count() == 0)
					Cleanup_ContainsOnlyArt.Add(dir);

			}
			else
			{
				while (dir.Parent.ContainsNoFiles())
					dir = dir.Parent;
				if (!Cleanup_EmptyDirs.Contains(dir))
					Cleanup_EmptyDirs.Add(dir);
			}
		}


		public void UpdateArtConsistency()
		{
			var directories = Songs.GroupBy(f => f.File.Directory.FullName.ToLower());
			var artD = ArtFiles.AsParallel().ToLookup(key => key.Directory.FullName.ToLower());

			foreach (var dir in directories)
			{
				var art = artD[dir.Key].ToList();
				foreach (var song in dir)
				{
					song.AccompanyingFiles = art;
					if (song.mp3.Tag.Pictures.Length == song.AccompanyingFiles.Count)
						song.AlbumArtConsistent = ArtConsistency.Consistent;
					else if (song.mp3.Tag.Pictures.Length == 0 && song.AccompanyingFiles.Count > 0)
						song.AlbumArtConsistent = ArtConsistency.FolderOnly;
					else if (song.mp3.Tag.Pictures.Length > 0 && song.AccompanyingFiles.Count == 0)
						song.AlbumArtConsistent = ArtConsistency.EmbeddedOnly;
					else
						song.AlbumArtConsistent = ArtConsistency.Inconsistent;
					song.Invalidate();
				}
			}
		}

		public int Count { get { return Songs.Count; } }
		public List<FileInfo> ExtraFiles { get { return ArtFiles; } }
	}

	public class ZetaFileAbstraction : TagLib.File.IFileAbstraction
	{
		private FileInfo fi;

		public ZetaFileAbstraction(FileInfo fi)
		{
			this.fi = fi;
		}

		public void CloseStream(System.IO.Stream stream)
		{
			if (stream != null)
				stream.Close();
		}

		public string Name { get { return fi.FullName; } }
		public System.IO.Stream ReadStream { get { return fi.Open(FileMode.Open, FileAccess.Read, FileShare.Read); } }
		public System.IO.Stream WriteStream { get { return fi.Open(FileMode.Open, FileAccess.ReadWrite); } }

	}

	public enum ArtConsistency
	{
		Consistent,
		FolderOnly,
		EmbeddedOnly,
		Inconsistent
	}

	public class Song
	{
		private FileInfo _fi;
		private ListViewItem _li;
		private SongCollection catalog;
		private System.Threading.CancellationToken ct;
		public Task loadTask;
		public bool Dirty;

		public Song(SongCollection catalog, FileInfo fi, System.Threading.CancellationToken asyncCancellationToken)
		{
			this.ct = asyncCancellationToken;
			this._fi = fi;
			this.catalog = catalog;
			if (asyncCancellationToken != null)
			{
				loadTask = new Task(Load, asyncCancellationToken);
				loadTask.Start();
			}
			else
			{
				Load();
			}
		}

		public void Load()
		{
			if (ct.IsCancellationRequested)
				return;
			try
			{
				mp3 = new TagLib.Mpeg.AudioFile(new ZetaFileAbstraction(_fi));
				HasEmbeddedArt = mp3.Tag.Pictures.Length > 0;
				Valid = true;

				// back-up original tags for change tracking
				OriginalTag = new TagLib.Id3v2.Tag();
				mp3.Tag.CopyTo(OriginalTag, true);

				// find "trim errors" - tracks that have extra spaces around the tags
				if (Title != null && Title.Trim() != Title) HasTrimError = true;
				else if (Album != null && Album.Trim() != Album) HasTrimError = true;
				HasTrimError = !(
						trimOK(mp3.Tag.Title) &&
						trimOK(mp3.Tag.Album) &&
						trimOK(mp3.Tag.Conductor) &&
						trimOK(mp3.Tag.Copyright) &&
						trimOK(mp3.Tag.Comment) &&
						trimOK(mp3.Tag.AlbumArtists) &&
						trimOK(mp3.Tag.Composers) &&
						trimOK(mp3.Tag.Performers) &&
						trimOK(mp3.Tag.Genres)
					);

				if (mp3.PossiblyCorrupt)
					Valid = false;

			}
			catch (TagLib.CorruptFileException x)
			{
				Valid = false;
				this.catalog.Cleanup_CorruptMusic.Add(this.File);
			}

			//var accItems = catalog.ExtraFiles.Where(f => f.Directory.FullName.Equals(_fi.Directory.FullName, StringComparison.OrdinalIgnoreCase)).ToList();
			//AccompanyingFiles = accItems.ToList();
			Invalidate();
		}

		private static bool trimOK(string str) 
		{
			return str == null || str == str.Trim();
		}
		private static bool trimOK(string[] str)
		{
			if (str == null) return true;
			foreach (var s in str)
				if (s != s.Trim())
					return false;
			return true;
		}

		public TagLib.Tag OriginalTag;
		public TagLib.Mpeg.AudioFile mp3;
		public FileInfo File { get { return _fi; } }
		public bool? HasEmbeddedArt;
		public ArtConsistency AlbumArtConsistent;
		public bool Valid;

		public string Title { get { return mp3 == null ? "?" : mp3.Tag.Title; } }
		public string Album { get { return mp3 == null ? "?" : mp3.Tag.Album; } }
		public string AlbumArtists { get { return mp3 == null ? "?" : mp3.Tag.JoinedAlbumArtists; } }
		public string Performers { get { return mp3 == null ? "?" : mp3.Tag.JoinedPerformers; } }


		public bool HasTrimError = false;
		public List<FileInfo> AccompanyingFiles = new List<FileInfo>();

		public void Invalidate()
		{
			_li = null;
		}

		public ListViewItem GetItem()
		{
			if (_li == null)
			{
				var li = new ListViewItem();
				li.ForeColor = System.Drawing.SystemColors.GrayText;

				loadTask.Wait();

				if (!Valid)
				{
					li.ForeColor = System.Drawing.Color.Red;
					if (mp3 != null)
						li.ToolTipText = String.Join("; ", mp3.CorruptionReasons);
				}

				li.Text = _fi.FullName;

				gsi(li, mp3.Tag.Title, OriginalTag.Title);
				gsi(li, mp3.Tag.JoinedAlbumArtists, OriginalTag.JoinedAlbumArtists);
				gsi(li, mp3.Tag.JoinedPerformers, OriginalTag.JoinedPerformers);
				gsi(li, mp3.Tag.Album, OriginalTag.Album);

				li.SubItems.Add(mp3.Tag.Pictures.Length + " items");
				li.SubItems.Add(AccompanyingFiles.Count + " items");
				li.SubItems.Add(AlbumArtConsistent.ToString());
				_li = li;
			}
			return _li;
		}

		private void gsi(ListViewItem li, string newstr, string oldstr)
		{
			li.ForeColor = System.Drawing.SystemColors.ControlText;
			li.UseItemStyleForSubItems = false;
			ListViewItem.ListViewSubItem si = new ListViewItem.ListViewSubItem(li, newstr);
			if (oldstr != null && oldstr.Trim() != oldstr)
			{
				li.ToolTipText = "Recommend: trim whitespace";
				si.ForeColor = System.Drawing.Color.Olive;
				si.Font = new Font(si.Font, FontStyle.Bold);
			}
			if (oldstr != null && newstr != oldstr)
			{
				si.ForeColor = System.Drawing.Color.Navy;
				si.Font = new Font(si.Font, FontStyle.Bold);
			}
			li.SubItems.Add(si);
		}

		private static void MarkTrimError(ListViewItem li, ListViewItem.ListViewSubItem si)
		{
			
		}
	}
}
