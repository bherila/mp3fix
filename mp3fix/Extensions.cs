﻿using Delimon.Win32.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mp3fix
{
	static class Extensions
	{

		public static bool ContainsNoFiles(this DirectoryInfo d)
		{
			if (d.GetFiles().Length > 0)
				return false;
			foreach (var sd in d.GetDirectories())
				if (!ContainsNoFiles(sd))
					return false;
			return true;
		}

		public static string ToSentenceCase(this string str)
		{
			StringBuilder sb = new StringBuilder(str.Length);
			bool cap = true; // first char is capitalized
			foreach (var c in str)
			{
				if (cap)
					sb.Append(Char.ToUpper(c));
				else
					sb.Append(Char.ToLower(c));

				if (Char.IsWhiteSpace(c))
					cap = true; // next char following whitespace is capitalized
				else
					cap = false;
			}
			return sb.ToString();
		}

	}
}
