﻿namespace mp3fix
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listView1 = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.button1 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.ck_variousartists = new System.Windows.Forms.CheckBox();
			this.ck_artistsort = new System.Windows.Forms.CheckBox();
			this.ck_genrecase = new System.Windows.Forms.CheckBox();
			this.ck_genresort = new System.Windows.Forms.CheckBox();
			this.ck_unifyamp = new System.Windows.Forms.CheckBox();
			this.button4 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.ck_fillemptyaa = new System.Windows.Forms.CheckBox();
			this.ck_trim = new System.Windows.Forms.CheckBox();
			this.ck_unifycase = new System.Windows.Forms.CheckBox();
			this.btnApply = new System.Windows.Forms.Button();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.lstCleanup = new System.Windows.Forms.ListView();
			this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.button8 = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.SuspendLayout();
			// 
			// listView1
			// 
			this.listView1.AllowColumnReorder = true;
			this.listView1.AllowDrop = true;
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
			this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listView1.FullRowSelect = true;
			this.listView1.GridLines = true;
			this.listView1.HideSelection = false;
			this.listView1.Location = new System.Drawing.Point(0, 0);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(720, 669);
			this.listView1.TabIndex = 0;
			this.listView1.UseCompatibleStateImageBehavior = false;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.VirtualMode = true;
			this.listView1.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listView1_ColumnClick);
			this.listView1.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.listView1_ItemDrag);
			this.listView1.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.listView1_RetrieveVirtualItem);
			this.listView1.SearchForVirtualItem += new System.Windows.Forms.SearchForVirtualItemEventHandler(this.listView1_SearchForVirtualItem);
			this.listView1.DragDrop += new System.Windows.Forms.DragEventHandler(this.listView1_DragDrop);
			this.listView1.DragEnter += new System.Windows.Forms.DragEventHandler(this.listView1_DragEnter);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Filename";
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Title";
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Album Artist";
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Artist";
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "Album";
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "Embedded Art";
			this.columnHeader6.Width = 87;
			// 
			// columnHeader7
			// 
			this.columnHeader7.Text = "Folder.jpg";
			this.columnHeader7.Width = 85;
			// 
			// columnHeader8
			// 
			this.columnHeader8.Text = "Art Consistent";
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(14, 461);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(163, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "Extract and Embed Art...";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button3
			// 
			this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.button3.Location = new System.Drawing.Point(14, 17);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(163, 23);
			this.button3.TabIndex = 3;
			this.button3.Text = "Load";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(14, 14);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(936, 701);
			this.tabControl1.TabIndex = 5;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.splitContainer2);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(928, 675);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Tag Issues";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// ck_variousartists
			// 
			this.ck_variousartists.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ck_variousartists.Location = new System.Drawing.Point(14, 287);
			this.ck_variousartists.Name = "ck_variousartists";
			this.ck_variousartists.Size = new System.Drawing.Size(163, 17);
			this.ck_variousartists.TabIndex = 16;
			this.ck_variousartists.Text = "Standardize \'Various Artists\'";
			this.ck_variousartists.UseVisualStyleBackColor = true;
			this.ck_variousartists.CheckedChanged += new System.EventHandler(this.updatechk);
			// 
			// ck_artistsort
			// 
			this.ck_artistsort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ck_artistsort.Location = new System.Drawing.Point(14, 264);
			this.ck_artistsort.Name = "ck_artistsort";
			this.ck_artistsort.Size = new System.Drawing.Size(163, 17);
			this.ck_artistsort.TabIndex = 15;
			this.ck_artistsort.Text = "Sort artists in tag";
			this.ck_artistsort.UseVisualStyleBackColor = true;
			this.ck_artistsort.CheckedChanged += new System.EventHandler(this.updatechk);
			// 
			// ck_genrecase
			// 
			this.ck_genrecase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ck_genrecase.Location = new System.Drawing.Point(14, 241);
			this.ck_genrecase.Name = "ck_genrecase";
			this.ck_genrecase.Size = new System.Drawing.Size(163, 17);
			this.ck_genrecase.TabIndex = 14;
			this.ck_genrecase.Text = "Correct genre casing";
			this.ck_genrecase.UseVisualStyleBackColor = true;
			this.ck_genrecase.CheckedChanged += new System.EventHandler(this.updatechk);
			// 
			// ck_genresort
			// 
			this.ck_genresort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ck_genresort.Location = new System.Drawing.Point(14, 218);
			this.ck_genresort.Name = "ck_genresort";
			this.ck_genresort.Size = new System.Drawing.Size(163, 17);
			this.ck_genresort.TabIndex = 13;
			this.ck_genresort.Text = "Sort genres in tag";
			this.ck_genresort.UseVisualStyleBackColor = true;
			this.ck_genresort.CheckedChanged += new System.EventHandler(this.updatechk);
			// 
			// ck_unifyamp
			// 
			this.ck_unifyamp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ck_unifyamp.Location = new System.Drawing.Point(14, 195);
			this.ck_unifyamp.Name = "ck_unifyamp";
			this.ck_unifyamp.Size = new System.Drawing.Size(163, 17);
			this.ck_unifyamp.TabIndex = 12;
			this.ck_unifyamp.Text = "Unify amp/and";
			this.ck_unifyamp.UseVisualStyleBackColor = true;
			this.ck_unifyamp.CheckedChanged += new System.EventHandler(this.updatechk);
			// 
			// button4
			// 
			this.button4.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.button4.Location = new System.Drawing.Point(95, 363);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(82, 23);
			this.button4.TabIndex = 11;
			this.button4.Text = "Select None";
			this.button4.UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this.button2.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.button2.Location = new System.Drawing.Point(15, 363);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(74, 23);
			this.button2.TabIndex = 10;
			this.button2.Text = "Select All";
			this.button2.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(15, 55);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(162, 47);
			this.label1.TabIndex = 9;
			this.label1.Text = "Tag corrections appear in bold and blue. Select the corrections you want to apply" +
    ".";
			// 
			// ck_fillemptyaa
			// 
			this.ck_fillemptyaa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ck_fillemptyaa.Location = new System.Drawing.Point(14, 172);
			this.ck_fillemptyaa.Name = "ck_fillemptyaa";
			this.ck_fillemptyaa.Size = new System.Drawing.Size(163, 17);
			this.ck_fillemptyaa.TabIndex = 8;
			this.ck_fillemptyaa.Text = "Fill Empty Album Artists";
			this.ck_fillemptyaa.UseVisualStyleBackColor = true;
			this.ck_fillemptyaa.CheckedChanged += new System.EventHandler(this.updatechk);
			// 
			// ck_trim
			// 
			this.ck_trim.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ck_trim.Location = new System.Drawing.Point(14, 149);
			this.ck_trim.Name = "ck_trim";
			this.ck_trim.Size = new System.Drawing.Size(163, 17);
			this.ck_trim.TabIndex = 7;
			this.ck_trim.Text = "Trim Whitespace";
			this.ck_trim.UseVisualStyleBackColor = true;
			this.ck_trim.CheckedChanged += new System.EventHandler(this.updatechk);
			// 
			// ck_unifycase
			// 
			this.ck_unifycase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ck_unifycase.Location = new System.Drawing.Point(14, 126);
			this.ck_unifycase.Name = "ck_unifycase";
			this.ck_unifycase.Size = new System.Drawing.Size(163, 17);
			this.ck_unifycase.TabIndex = 6;
			this.ck_unifycase.Text = "Unify Cases";
			this.ck_unifycase.UseVisualStyleBackColor = true;
			this.ck_unifycase.CheckedChanged += new System.EventHandler(this.updatechk);
			// 
			// btnApply
			// 
			this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.btnApply.Location = new System.Drawing.Point(14, 334);
			this.btnApply.Name = "btnApply";
			this.btnApply.Size = new System.Drawing.Size(163, 23);
			this.btnApply.TabIndex = 5;
			this.btnApply.Text = "Save Selected Suggestions";
			this.btnApply.UseVisualStyleBackColor = true;
			this.btnApply.Click += new System.EventHandler(this.button2_Click_1);
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.splitContainer1);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(928, 675);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Disk Cleanup";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.splitContainer1.Location = new System.Drawing.Point(3, 3);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.lstCleanup);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.button8);
			this.splitContainer1.Size = new System.Drawing.Size(922, 669);
			this.splitContainer1.SplitterDistance = 746;
			this.splitContainer1.TabIndex = 0;
			// 
			// lstCleanup
			// 
			this.lstCleanup.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10});
			this.lstCleanup.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstCleanup.Location = new System.Drawing.Point(0, 0);
			this.lstCleanup.Name = "lstCleanup";
			this.lstCleanup.Size = new System.Drawing.Size(746, 669);
			this.lstCleanup.TabIndex = 1;
			this.lstCleanup.UseCompatibleStateImageBehavior = false;
			this.lstCleanup.View = System.Windows.Forms.View.Details;
			this.lstCleanup.ItemActivate += new System.EventHandler(this.lstCleanup_ItemActivate);
			// 
			// columnHeader9
			// 
			this.columnHeader9.Text = "Path";
			// 
			// columnHeader10
			// 
			this.columnHeader10.Text = "Reason";
			// 
			// button8
			// 
			this.button8.Location = new System.Drawing.Point(3, 3);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(166, 23);
			this.button8.TabIndex = 0;
			this.button8.Text = "Delete Selected";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new System.EventHandler(this.button8_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button3);
			this.panel1.Controls.Add(this.ck_variousartists);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Controls.Add(this.ck_artistsort);
			this.panel1.Controls.Add(this.btnApply);
			this.panel1.Controls.Add(this.ck_genrecase);
			this.panel1.Controls.Add(this.ck_unifycase);
			this.panel1.Controls.Add(this.ck_genresort);
			this.panel1.Controls.Add(this.ck_trim);
			this.panel1.Controls.Add(this.ck_unifyamp);
			this.panel1.Controls.Add(this.ck_fillemptyaa);
			this.panel1.Controls.Add(this.button4);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.button2);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(198, 669);
			this.panel1.TabIndex = 17;
			// 
			// splitContainer2
			// 
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.splitContainer2.Location = new System.Drawing.Point(3, 3);
			this.splitContainer2.Name = "splitContainer2";
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.listView1);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.panel1);
			this.splitContainer2.Size = new System.Drawing.Size(922, 669);
			this.splitContainer2.SplitterDistance = 720;
			this.splitContainer2.TabIndex = 18;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(964, 729);
			this.Controls.Add(this.tabControl1);
			this.Name = "Form1";
			this.Padding = new System.Windows.Forms.Padding(14);
			this.Text = "Ben Herila\'s mp3fix";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.ColumnHeader columnHeader7;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.ColumnHeader columnHeader8;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.ListView lstCleanup;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.ColumnHeader columnHeader9;
		private System.Windows.Forms.ColumnHeader columnHeader10;
		private System.Windows.Forms.CheckBox ck_fillemptyaa;
		private System.Windows.Forms.CheckBox ck_trim;
		private System.Windows.Forms.CheckBox ck_unifycase;
		private System.Windows.Forms.Button btnApply;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.CheckBox ck_unifyamp;
		private System.Windows.Forms.CheckBox ck_genrecase;
		private System.Windows.Forms.CheckBox ck_variousartists;
		private System.Windows.Forms.CheckBox ck_artistsort;
		private System.Windows.Forms.CheckBox ck_genresort;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.Panel panel1;
	}
}

